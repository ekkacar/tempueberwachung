#include "OneWire.h"                      //Datenbank einbinden
#include "DallasTemperature.h"

#define ONE_WIRE_BUS 2                    //One Wire Bus definieren 
OneWire oneWire(ONE_WIRE_BUS); 
DallasTemperature sensors(&oneWire);



int i = 0;

struct messtyp                            // struct erstellen 
{
  float Sensor_1;
  float Sensor_2;
  long  millis;
};


messtyp messdaten[10];                    //Array definieren


void setup()
{
 
  Serial.begin(9600);                     //Serielleverbindung zum "Monitor" 9600 ist die Baudrate
  sensors.begin();                        //Sensor "starten"
} 

void loop()
{
  sensors.requestTemperatures();                                        //Sensor "auslesen" anfordern
  for (int i = 0; i < 10; i++)                                          //forschleife für das Array 
  {
                            
 
        messdaten[i].Sensor_1 = (sensors.getTempCByIndex(0));           //Sensor 1 auslesen und in die entsprechende Position des struct ins Array schreiben
        messdaten[i].Sensor_2 = (sensors.getTempCByIndex(1));           //Sensor 2 auslesen und in die entsprechende Position des struct ins Array schreiben
        messdaten[i].millis   = millis();                               //millis in die entsprechende Position des struct ins Array schreiben 

        Serial.print(messdaten[i].Sensor_1);                            //Ausgabe
        Serial.print(messdaten[i].Sensor_2);
        Serial.print(messdaten[i].millis);
        
      
         
       
        delay(2000);                                                    //Wartezeit
   }
}        
